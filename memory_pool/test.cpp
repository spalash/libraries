#include "memory_pool.h"
#include <iostream>

int main(){
  Memory_Pool pool;
  double* slot=pool.get_slot();
  *slot = 1;
  std::cout<<*slot<<"\n";
  pool.return_slot(slot);
  slot=pool.get_slot();
  std::cout<<*slot<<"\n";
  *slot = 2;
  std::cout<<*slot<<"\n";
  return 1;
}
#include <iostream>
#include <vector>
#include <cstdlib>

#define NEW_POOL_SIZE 1000

class Memory_Pool{
private:
	std::vector<double*> *pool;
	std::vector<double*> *pointers;
	void add_memory(){
		int i;
		double *new_pool=(double*)malloc(NEW_POOL_SIZE*sizeof(double));
		for(i=0;i<NEW_POOL_SIZE;i++)
		pool->push_back(&new_pool[i]);
		pointers->push_back(new_pool);
	}

public:

	Memory_Pool(){
		pool=new std::vector<double*>();
		pointers=new std::vector<double*>();
		add_memory();
	}

	double* get_slot(){
		double *slot;
		if(pool->empty())
			add_memory();
		slot = pool->at(pool->size()-1);
		pool->pop_back();
		return slot;
	}

	void return_slot(double *slot){
		pool->push_back(slot);
	}

	void free_pool_memory(){
		int i;
		for(i=0;i<pointers->size();i++)
			free(pointers->at(i));
	}		

};

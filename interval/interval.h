#include <algorithm>
#include <cstdlib>
#include <iostream>

#define SET_SIZE 3
#define SET_BUFFER_SIZE 1

class Interval_Set {

private:
	double *start_points;
	double *end_points;
	int size;

public:
	Interval_Set(){
		start_points = (double*)malloc(sizeof(double)*(SET_SIZE+SET_BUFFER_SIZE));
		end_points = (double*)malloc(sizeof(double)*(SET_SIZE+SET_BUFFER_SIZE));
		size=0;
	}

	double add_interval(double start, double end){
		if(end-start==0)
			return 0;
		if(size==0){
			start_points[0]=start;
			end_points[0]=end;
			size++;
			return end-start;
		}
		int right = std::upper_bound(start_points, start_points+size,end) - start_points;
		int left = std::lower_bound(end_points, end_points+size,start) - end_points;

		int i;
		int temp;

		if(right==left){
			if(size==SET_SIZE){
				for(i=1;i<=right-1;i++)
					start_points[i-1]=start_points[i];
				for(i=1;i<=left-1;i++)
					end_points[i-1]=end_points[i];
				end_points[left-1]=end;
				start_points[right-1]=start;
			}
			else {
				for(i=size;i>=right;i--)
					start_points[i]=start_points[i-1];
				for(i=size;i>=left;i--)
					end_points[i]=end_points[i-1];
				end_points[left]=end;
				start_points[right]=start;
				size++;	
			}
			return end-start;
		}

		float sum_intervals=0;
		int diff=right-left-1;

		for(i=left;i<right;i++)
			sum_intervals+=(end_points[i]-start_points[i]);

		if(left!=0)
			start_points[left]=start_points[left]<start?start_points[left]:start;
		end_points[left]=end_points[right-1]>end?end_points[right-1]:end;

		for(i=right;i<size;i++){
			start_points[i-diff]=start_points[i];
			end_points[i-diff]=end_points[i];
		}

		size-=diff;
		return end_points[left]-start_points[left]-sum_intervals;

	}

	void print_interval_set(){
		for(int i=0;i<size;i++){
			std::cout<<"( "<<start_points[i]<<", "<<end_points[i]<<" )\n";	
		}
	}	

};